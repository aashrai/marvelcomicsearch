package aashrai.com.marvelcomicsearch.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import aashrai.com.marvelcomicsearch.network.dto.Thumbnail;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class Utils {

  public static final int SEARCH_PAGE_SIZE = 10;

  public static String generateHash (long timestamp, String publicKey, String privateKey) {
    try {
      String value = timestamp + privateKey + publicKey;
      MessageDigest md5Encoder = MessageDigest.getInstance("MD5");
      byte[] md5Bytes = md5Encoder.digest(value.getBytes());

      StringBuilder md5 = new StringBuilder();
      for (int i = 0; i < md5Bytes.length; ++i) {
        md5.append(Integer.toHexString((md5Bytes[i] & 0xFF) | 0x100).substring(1, 3));
      }
      return md5.toString();
    } catch (NoSuchAlgorithmException e) {
      return null;
    }
  }

  public static String getThumbnailUrl (Thumbnail thumbnail) {
    return thumbnail.getPath() + "." + thumbnail.getExtension();
  }
}
