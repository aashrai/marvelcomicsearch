package aashrai.com.marvelcomicsearch.adapters;

import android.support.v7.util.DiffUtil;

import java.util.List;

import aashrai.com.marvelcomicsearch.network.dto.ComicResult;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class CustomDiffUtil extends DiffUtil.Callback {

  private final List<ComicResult> oldItems;
  private final List<ComicResult> newItems;

  public CustomDiffUtil (List<ComicResult> oldItems, List<ComicResult> newItems) {
    this.oldItems = oldItems;
    this.newItems = newItems;
  }

  @Override
  public int getOldListSize () {
    return oldItems.size();
  }

  @Override
  public int getNewListSize () {
    return newItems.size();
  }

  @Override
  public boolean areItemsTheSame (int oldItemPosition, int newItemPosition) {
    return oldItems.get(oldItemPosition).getId() == newItems.get(newItemPosition).getId();
  }

  @Override
  public boolean areContentsTheSame (int oldItemPosition, int newItemPosition) {
    return true;
  }
}
