package aashrai.com.marvelcomicsearch.adapters;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import aashrai.com.marvelcomicsearch.R;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class CustomDecorator extends RecyclerView.ItemDecoration {
  @Override
  public void getItemOffsets (Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
    int margin = view.getContext().getResources().getDimensionPixelOffset(R.dimen.margin_half);
    outRect.left = margin;
    outRect.right = margin;
    outRect.top = margin;
    if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1)
      outRect.bottom = margin;
  }
}
