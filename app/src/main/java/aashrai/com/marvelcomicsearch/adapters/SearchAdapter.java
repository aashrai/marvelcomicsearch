package aashrai.com.marvelcomicsearch.adapters;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import aashrai.com.marvelcomicsearch.R;
import aashrai.com.marvelcomicsearch.network.dto.ComicResult;
import aashrai.com.marvelcomicsearch.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private List<ComicResult> items = new ArrayList<>();
  private final int SEARCH_ITEM = 0;
  private final int PROGRESS_ITEM = 1;
  private boolean showProgressBar = false;

  public interface onItemClickListener {
    void onItemClick (ComicResult comicResult);
  }

  public void setOnItemClickListener (SearchAdapter.onItemClickListener onItemClickListener) {
    this.onItemClickListener = onItemClickListener;
  }

  private onItemClickListener onItemClickListener;

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
    if (viewType == SEARCH_ITEM)
      return new ResultViewHolder(LayoutInflater.from(parent.getContext())
              .inflate(R.layout.item_search_result, parent, false));
    return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_search_progress_bar, parent, false));
  }

  @Override
  public void onBindViewHolder (RecyclerView.ViewHolder holder, int position) {
    if (getItemViewType(position) == SEARCH_ITEM) {
      ResultViewHolder viewHolder = ((ResultViewHolder) holder);
      ComicResult comicResult = items.get(position);
      Picasso.with(viewHolder.thumbnail.getContext())
              .load(Utils.getThumbnailUrl(comicResult.getThumbnail()))
              .into(viewHolder.thumbnail);
      viewHolder.title.setText(comicResult.getTitle());
      viewHolder.subtitle.setText(comicResult.getPrices().get(0).getPrice());
    }
  }

  @Override
  public int getItemCount () {
    return items.size() + (showProgressBar ? 1 : 0);
  }

  @Override
  public int getItemViewType (int position) {
    if (position == items.size())
      return PROGRESS_ITEM;
    return SEARCH_ITEM;
  }

  public void addProgressBar () {
    showProgressBar = true;
    notifyItemInserted(items.size());
  }

  public void addNewItems (List<ComicResult> items) {
    List<ComicResult> oldItems = new ArrayList<>(this.items);
    this.items.addAll(items);
    updateItems(this.items, oldItems);
  }

  public void removeProgressBar () {
    showProgressBar = false;
    notifyItemRemoved(items.size());
  }


  public void addAll (List<ComicResult> items) {
    List<ComicResult> oldItems = new ArrayList<>(this.items);
    this.items.clear();
    this.items.addAll(items);
    updateItems(this.items, oldItems);
  }

  private void updateItems (List<ComicResult> newItems, List<ComicResult> oldItems) {
    DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new CustomDiffUtil(oldItems, newItems));
    diffResult.dispatchUpdatesTo(this);
  }

  private class ProgressViewHolder extends RecyclerView.ViewHolder {
    ProgressViewHolder (View itemView) {
      super(itemView);
    }
  }

  class ResultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView (R.id.iv_search_result_thumbnail)
    public ImageView thumbnail;
    @BindView (R.id.tv_search_result_title)
    public TextView title;
    @BindView (R.id.tv_search_result_subtitle)
    public TextView subtitle;

    public ResultViewHolder (View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      ButterKnife.bind(this, itemView);
    }

    @Override
    public void onClick (View view) {
      if (onItemClickListener != null)
        onItemClickListener.onItemClick(items.get(getAdapterPosition()));
    }
  }
}
