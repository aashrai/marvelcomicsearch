package aashrai.com.marvelcomicsearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import aashrai.com.marvelcomicsearch.network.dto.ComicResult;
import aashrai.com.marvelcomicsearch.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class FragmentDetail extends Fragment {

  @BindView (R.id.iv_search_detail_thumbnail)
  ImageView ivSearchDetailThumbnail;
  @BindView (R.id.tv_search_detail_title)
  TextView tvSearchDetailTitle;
  @BindView (R.id.tv_search_detail_subtitle)
  TextView tvSearchDetailSubtitle;
  @BindView (R.id.tv_search_result_isbn)
  TextView tvSearchResultIsbn;
  @BindView (R.id.tv_search_detail_desc)
  TextView tvSearchDetailDesc;
  @BindView (R.id.tv_search_detail_creators)
  TextView tvSearchDetailCreators;
  @BindView (R.id.tv_search_detail_characters)
  TextView tvSearchDetailCharacters;

  @Nullable
  @Override
  public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View layout = inflater.inflate(R.layout.fragment_detail, container, false);
    ButterKnife.bind(this, layout);
    return layout;
  }

  @Override
  public void onActivityCreated (@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (getActivity() != null)
      setData(((DataCommunicator) getActivity()).getSelectedComic());
  }

  private void setData (ComicResult comicResult) {
    Picasso.with(getContext()).load(Utils.getThumbnailUrl(comicResult.getThumbnail()))
            .into(ivSearchDetailThumbnail);
    tvSearchDetailTitle.setText(comicResult.getTitle());
    tvSearchDetailSubtitle.setText(comicResult.getPrices().get(0).getPrice());
    tvSearchResultIsbn.setText(comicResult.getIsbn());
    tvSearchDetailDesc.setText(comicResult.getDescription());
    tvSearchDetailCreators.setText(comicResult.getCreators().toString());
    tvSearchDetailCharacters.setText(comicResult.getCharacters().toString());
  }

  public interface DataCommunicator {
    ComicResult getSelectedComic ();
  }
}
