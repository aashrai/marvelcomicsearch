package aashrai.com.marvelcomicsearch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.widget.FrameLayout;

import aashrai.com.marvelcomicsearch.network.dto.ComicResult;
import aashrai.com.marvelcomicsearch.network.dto.ComicSearch;
import aashrai.com.marvelcomicsearch.utils.Utils;
import butterknife.BindView;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements FragmentSearch.DataCommunicator,
        FragmentDetail.DataCommunicator {

  private final String publicKey = "13a22d4e63071ec92ff06a087add8e9e";
  private final String privateKey = "6c33b3fefb92c04b04bf87d8ecd4f7db46a525a3";
  @BindView (R.id.fragment_holder)
  FrameLayout fragmentHolder;
  ComicResult selectedComic;

  static {
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
  }

  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    addFragment(new FragmentSearch(), false);
  }

  @Override
  public void onSearch (String query, Callback<ComicSearch> searchCallback) {
    fetchComics(query, Utils.SEARCH_PAGE_SIZE, 0, searchCallback);
  }

  private void fetchComics (String query, int pageSize, int offset,
                            Callback<ComicSearch> searchCallback) {
    long timestamp = System.currentTimeMillis();
    String hash = Utils.generateHash(timestamp, publicKey, privateKey);
    ComicApplication.getComicService()
            .getComics(query, pageSize, offset, publicKey, hash, timestamp)
            .enqueue(searchCallback);
  }

  @Override
  public void fetchNextContent (String query, int limit, int offset,
                                Callback<ComicSearch> searchCallback) {
    fetchComics(query, limit, offset, searchCallback);
  }

  @Override
  public void onComicSelected (ComicResult selectedComic) {
    this.selectedComic = selectedComic;
    addFragment(new FragmentDetail(), true);
  }

  public void addFragment (Fragment fragment, boolean addtoBackstack) {
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction()
            .add(R.id.fragment_holder, fragment);
    if (addtoBackstack)
      fragmentTransaction.addToBackStack(null);
    fragmentTransaction.commit();
  }

  @Override
  public ComicResult getSelectedComic () {
    return selectedComic;
  }
}
