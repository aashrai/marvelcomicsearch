package aashrai.com.marvelcomicsearch.network.dto;

import java.util.ArrayList;

public class Characters {

  private ArrayList<Item> items;

  public ArrayList<Item> getItems () {
    return this.items;
  }

  private class Item {
    private String resourceURI;

    public String getResourceURI () {
      return this.resourceURI;
    }

    private String name;

    public String getName () {
      return this.name;
    }

    @Override
    public String toString () {
      return name;
    }
  }

  @Override
  public String toString () {
    if (items.size() == 0)
      return "";
    StringBuffer stringBuffer = new StringBuffer("");
    for (int i = 0; i < items.size() - 1; i++) {
      stringBuffer.append(items.get(i).toString() + "\n");
    }
    stringBuffer.append(items.get(items.size() - 1).toString());
    return stringBuffer.toString();
  }
}