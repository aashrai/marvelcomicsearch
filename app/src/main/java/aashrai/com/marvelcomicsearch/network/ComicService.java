package aashrai.com.marvelcomicsearch.network;

import aashrai.com.marvelcomicsearch.network.dto.ComicSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public interface ComicService {
  @GET ("comics")
  Call<ComicSearch> getComics (@Query ("titleStartsWith") String searchQuery,
                               @Query ("limit") int limit,
                               @Query ("offset") int offset,
                               @Query ("apikey") String apiKey,
                               @Query ("hash") String hash,
                               @Query ("ts") long timestamp);
}
