package aashrai.com.marvelcomicsearch.network.dto;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class ComicPrice {
  private String type;

  public String getType () {
    return this.type;
  }

  public void setType (String type) {
    this.type = type;
  }

  private double price;

  public String getPrice () {
    return "Price: $" + this.price;
  }

  public void setPrice (double price) {
    this.price = price;
  }
}
