package aashrai.com.marvelcomicsearch.network.dto;

import java.util.ArrayList;

public class Creators {

  private ArrayList<Item> items;

  public ArrayList<Item> getItems () {
    return this.items;
  }

  private class Item {
    private String resourceURI;

    public String getResourceURI () {
      return this.resourceURI;
    }

    private String name;

    public String getName () {
      return this.name;
    }

    public void setName (String name) {
      this.name = name;
    }

    private String role;

    public String getRole () {
      return this.role;
    }

    @Override
    public String toString () {
      return name + (role != null && !role.isEmpty() ? "(" + role + ")" : "");
    }
  }

  @Override
  public String toString () {
    if (getItems().size() == 0)
      return "";

    StringBuffer stringBuilder = new StringBuffer("");
    for (int i = 0; i < getItems().size() - 1; i++) {
      stringBuilder.append(getItems().get(i).toString() + "\n");
    }
    stringBuilder.append(getItems().get(getItems().size() - 1).toString());
    return stringBuilder.toString();
  }
}