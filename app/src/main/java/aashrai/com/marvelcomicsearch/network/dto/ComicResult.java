package aashrai.com.marvelcomicsearch.network.dto;

import java.util.ArrayList;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class ComicResult {
  private int id;

  public int getId () {
    return this.id;
  }

  public void setId (int id) {
    this.id = id;
  }

  private String title;

  public String getTitle () {
    return this.title;
  }

  private String description;

  public String getDescription () {
    return this.description;
  }

  private String isbn;

  public String getIsbn () {
    return "ISBN: " + (isbn != null && !isbn.isEmpty() ? isbn : "NOT AVAILABLE");
  }

  private int pageCount;

  public int getPageCount () {
    return this.pageCount;
  }

  private String resourceURI;

  public String getResourceURI () {
    return this.resourceURI;
  }

  private ArrayList<ComicPrice> prices;

  public ArrayList<ComicPrice> getPrices () {
    return this.prices;
  }

  private Thumbnail thumbnail;

  public Thumbnail getThumbnail () {
    return this.thumbnail;
  }

  private ArrayList<ComicImage> images;

  public ArrayList<ComicImage> getImages () {
    return this.images;
  }

  private Creators creators;

  public Creators getCreators () {
    return this.creators;
  }

  private Characters characters;

  public Characters getCharacters () {
    return this.characters;
  }
}
