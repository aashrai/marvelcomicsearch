package aashrai.com.marvelcomicsearch.network.dto;

import java.util.ArrayList;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class ComicData {
  private int offset;

  public int getOffset () {
    return this.offset;
  }

  private int limit;

  public int getLimit () {
    return this.limit;
  }

  private int total;

  public int getTotal () {
    return this.total;
  }

  private int count;

  public int getCount () {
    return this.count;
  }

  private ArrayList<ComicResult> results;

  public ArrayList<ComicResult> getResults () {
    return this.results;
  }

}
