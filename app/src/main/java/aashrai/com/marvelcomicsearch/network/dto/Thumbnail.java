package aashrai.com.marvelcomicsearch.network.dto;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class Thumbnail {
  private String path;

  public String getPath () {
    return this.path;
  }

  private String extension;

  public String getExtension () {
    return this.extension;
  }
}
