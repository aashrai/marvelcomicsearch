package aashrai.com.marvelcomicsearch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import aashrai.com.marvelcomicsearch.adapters.CustomDecorator;
import aashrai.com.marvelcomicsearch.adapters.SearchAdapter;
import aashrai.com.marvelcomicsearch.network.dto.ComicData;
import aashrai.com.marvelcomicsearch.network.dto.ComicResult;
import aashrai.com.marvelcomicsearch.network.dto.ComicSearch;
import aashrai.com.marvelcomicsearch.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class FragmentSearch extends Fragment implements SearchAdapter.onItemClickListener {

  @BindView (R.id.et_search_bar)
  EditText searchBar;
  @BindView (R.id.rv_search_result)
  RecyclerView searchResult;
  @BindView (R.id.pb_search)
  ProgressBar progressBar;
  SearchAdapter searchAdapter;
  boolean newContentLoading = false;
  private static final String TAG = "FragmentSearch";
  private ComicData comicDataList;
  int searchOffsetMultiple = 1;

  @Nullable
  @Override
  public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {
    View layout = inflater.inflate(R.layout.fragment_search, container, false);
    ButterKnife.bind(this, layout);
    configureRecyclerView();
    handleSearch();
    return layout;
  }

  private void handleSearch () {
    setCompoundDrawable();
    searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction (TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
          if (getActivity() != null) {
            ((DataCommunicator) getActivity()).onSearch(getSearchQuery(), new SearchCallback());

            progressBar.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
            return true;
          }
        }
        return false;
      }
    });
  }

  private void setCompoundDrawable () {
    searchBar.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_black, 0, 0, 0);
  }

  private void configureRecyclerView () {
    searchAdapter = new SearchAdapter();
    searchResult.setAdapter(searchAdapter);
    searchResult.setLayoutManager(new LinearLayoutManager(getContext(),
            LinearLayoutManager.VERTICAL, false));
    searchResult.addItemDecoration(new CustomDecorator());
    searchAdapter.setOnItemClickListener(this);

    searchResult.addOnScrollListener(new ScrollListener());
  }

  private class ScrollListener extends RecyclerView.OnScrollListener {
    @Override
    public void onScrolled (RecyclerView recyclerView, int dx, int dy) {
      super.onScrolled(recyclerView, dx, dy);
      LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

      if (layoutManager.findLastVisibleItemPosition() >= recyclerView.getAdapter().getItemCount() - 3
              && !newContentLoading) {
        ((DataCommunicator) getActivity()).fetchNextContent(getSearchQuery(), Utils.SEARCH_PAGE_SIZE,
                searchOffsetMultiple * (comicDataList.getOffset() + Utils.SEARCH_PAGE_SIZE + 1),
                new NewContentCallback());
        newContentLoading = true;
        searchAdapter.addProgressBar();
        ++searchOffsetMultiple;
      }

    }
  }

  @NonNull
  private String getSearchQuery () {
    return searchBar.getText().toString().trim();
  }

  private class SearchCallback implements Callback<ComicSearch> {
    @Override
    public void onResponse (Call<ComicSearch> call, Response<ComicSearch> response) {
      progressBar.setVisibility(View.GONE);
      comicDataList = response.body().getData();
      searchAdapter.addAll(response.body().getData().getResults());
    }

    @Override
    public void onFailure (Call<ComicSearch> call, Throwable t) {
      progressBar.setVisibility(View.GONE);
      Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
    }
  }

  private class NewContentCallback implements Callback<ComicSearch> {

    @Override
    public void onResponse (Call<ComicSearch> call, Response<ComicSearch> response) {
      newContentLoading = false;
      searchAdapter.removeProgressBar();
      searchAdapter.addNewItems(response.body().getData().getResults());
      Log.d(TAG, "onResponse: ");
    }

    @Override
    public void onFailure (Call<ComicSearch> call, Throwable t) {
      newContentLoading = false;
      Log.d(TAG, "onFailure: ");

    }
  }

  @Override
  public void onItemClick (ComicResult comicResult) {
    ((DataCommunicator) getActivity()).onComicSelected(comicResult);
  }

  public interface DataCommunicator {
    void onSearch (String query, Callback<ComicSearch> searchCallback);

    void fetchNextContent (String query, int limit, int offset, Callback<ComicSearch> searchCallback);

    void onComicSelected (ComicResult comicResult);
  }
}
