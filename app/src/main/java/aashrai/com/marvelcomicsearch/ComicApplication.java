package aashrai.com.marvelcomicsearch;

import android.app.Application;

import aashrai.com.marvelcomicsearch.network.ComicService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aashrairavooru on 19/01/17.
 */

public class ComicApplication extends Application {

  private static ComicService comicService;

  @Override
  public void onCreate () {
    super.onCreate();

    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    logging.setLevel(Level.BODY);
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(logging)
            .build();

    comicService = new Retrofit.Builder().baseUrl("https://gateway.marvel.com:443/v1/public/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build().create(ComicService.class);
  }

  public static ComicService getComicService () {
    return comicService;
  }
}
