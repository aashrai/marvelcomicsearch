package aashrai.com.marvelcomicsearch;

public class SampleData {

  public static final String sampleComicResponse="{\n" +
          "    \"code\": 200,\n" +
          "    \"status\": \"Ok\",\n" +
          "    \"copyright\": \"© 2017 MARVEL\",\n" +
          "    \"attributionText\": \"Data provided by Marvel. © 2017 MARVEL\",\n" +
          "    \"attributionHTML\": \"<a href=\\\"http://marvel.com\\\">Data provided by Marvel. © 2017 MARVEL</a>\",\n" +
          "    \"etag\": \"3438fcdeaa2db0b2b28887b06f05338fba6465d7\",\n" +
          "    \"data\": {\n" +
          "        \"offset\": 0,\n" +
          "        \"limit\": 20,\n" +
          "        \"total\": 1143,\n" +
          "        \"count\": 20,\n" +
          "        \"results\": \n" +
          "        [{\n" +
          "            \"id\": 60472,\n" +
          "            \"digitalId\": 0,\n" +
          "            \"title\": \"Spider-Gwen Vol. 1 (Hardcover)\",\n" +
          "            \"issueNumber\": 0,\n" +
          "            \"variantDescription\": \"\",\n" +
          "            \"description\": \"Gwen Stacy is the Spider-Woman of her world, but you knew that already. What you don’t know is what’s waiting for her on the other side of the SPIDER-VERSE! On the most tragic day of her life, Gwen was convinced that the Lizard died in her arms along with Peter Parker. But a new reptilian rampage leaves her in doubt — not only about Peter’s life, but also his death. And Gwen’s troubles pile up as the Osborns of her world make their debut, and she finds herself on S.H.I.E.L.D.’s most wanted list! Perhaps another Spider-Woman, Jessica Drew, could share some lessons about power and responsibility! Collecting EDGE OF SPIDER-VERSE #2, SPIDER-GWEN (2015A) #1-5 and SPIDER-GWEN (2015B) #1-6.\",\n" +
          "            \"modified\": \"2016-08-02T11:31:04-0400\",\n" +
          "            \"isbn\": \"978-1-302-90371-8\",\n" +
          "            \"upc\": \"\",\n" +
          "            \"diamondCode\": \"\",\n" +
          "            \"ean\": \"9781302 903718 53499\",\n" +
          "            \"issn\": \"\",\n" +
          "            \"format\": \"Hardcover\",\n" +
          "            \"pageCount\": 272,\n" +
          "            \"textObjects\": [{\n" +
          "                \"type\": \"issue_solicit_text\",\n" +
          "                \"language\": \"en-us\",\n" +
          "                \"text\": \"Gwen Stacy is the Spider-Woman of her world, but you knew that already. What you don’t know is what’s waiting for her on the other side of the SPIDER-VERSE! On the most tragic day of her life, Gwen was convinced that the Lizard died in her arms along with Peter Parker. But a new reptilian rampage leaves her in doubt — not only about Peter’s life, but also his death. And Gwen’s troubles pile up as the Osborns of her world make their debut, and she finds herself on S.H.I.E.L.D.’s most wanted list! Perhaps another Spider-Woman, Jessica Drew, could share some lessons about power and responsibility! Collecting EDGE OF SPIDER-VERSE #2, SPIDER-GWEN (2015A) #1-5 and SPIDER-GWEN (2015B) #1-6.\"\n" +
          "            }],\n" +
          "            \"resourceURI\": \"http://gateway.marvel.com/v1/public/comics/60472\",\n" +
          "            \"urls\": [{\n" +
          "                \"type\": \"detail\",\n" +
          "                \"url\": \"http://marvel.com/comics/collection/60472/spider-gwen_vol_1_hardcover?utm_campaign=apiRef&utm_source=13a22d4e63071ec92ff06a087add8e9e\"\n" +
          "            }],\n" +
          "            \"series\": {\n" +
          "                \"resourceURI\": \"http://gateway.marvel.com/v1/public/series/22351\",\n" +
          "                \"name\": \"Spider-Gwen Vol. 1 (2017)\"\n" +
          "            },\n" +
          "            \"variants\": [],\n" +
          "            \"collections\": [],\n" +
          "            \"collectedIssues\": [],\n" +
          "            \"dates\": [{\n" +
          "                \"type\": \"onsaleDate\",\n" +
          "                \"date\": \"2017-02-01T00:00:00-0500\"\n" +
          "            }, {\n" +
          "                \"type\": \"focDate\",\n" +
          "                \"date\": \"2016-12-13T00:00:00-0500\"\n" +
          "            }],\n" +
          "            \"prices\": [{\n" +
          "                \"type\": \"printPrice\",\n" +
          "                \"price\": 34.99\n" +
          "            }],\n" +
          "            \"thumbnail\": {\n" +
          "                \"path\": \"http://i.annihil.us/u/prod/marvel/i/mg/8/d0/57a0bd3031f6f\",\n" +
          "                \"extension\": \"jpg\"\n" +
          "            },\n" +
          "            \"images\": [{\n" +
          "                \"path\": \"http://i.annihil.us/u/prod/marvel/i/mg/8/d0/57a0bd3031f6f\",\n" +
          "                \"extension\": \"jpg\"\n" +
          "            }],\n" +
          "            \"creators\": {\n" +
          "                \"available\": 4,\n" +
          "                \"collectionURI\": \"http://gateway.marvel.com/v1/public/comics/60472/creators\",\n" +
          "                \"items\": [{\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/11306\",\n" +
          "                    \"name\": \"Jason Latour\",\n" +
          "                    \"role\": \"writer\"\n" +
          "                }, {\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/12399\",\n" +
          "                    \"name\": \"Robbi Rodriguez\",\n" +
          "                    \"role\": \"penciller (cover)\"\n" +
          "                }, {\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/12848\",\n" +
          "                    \"name\": \"Chris Visions\",\n" +
          "                    \"role\": \"penciller\"\n" +
          "                }, {\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/creators/4430\",\n" +
          "                    \"name\": \"Jeff Youngquist\",\n" +
          "                    \"role\": \"editor\"\n" +
          "                }],\n" +
          "                \"returned\": 4\n" +
          "            },\n" +
          "            \"characters\": {\n" +
          "                \"available\": 0,\n" +
          "                \"collectionURI\": \"http://gateway.marvel.com/v1/public/comics/60472/characters\",\n" +
          "                \"items\": [],\n" +
          "                \"returned\": 0\n" +
          "            },\n" +
          "            \"stories\": {\n" +
          "                \"available\": 2,\n" +
          "                \"collectionURI\": \"http://gateway.marvel.com/v1/public/comics/60472/stories\",\n" +
          "                \"items\": [{\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/stories/131413\",\n" +
          "                    \"name\": \"cover from Spider-Gwen (2017)\",\n" +
          "                    \"type\": \"cover\"\n" +
          "                }, {\n" +
          "                    \"resourceURI\": \"http://gateway.marvel.com/v1/public/stories/131414\",\n" +
          "                    \"name\": \"story from Spider-Gwen (2017)\",\n" +
          "                    \"type\": \"interiorStory\"\n" +
          "                }],\n" +
          "                \"returned\": 2\n" +
          "            },\n" +
          "            \"events\": {\n" +
          "                \"available\": 0,\n" +
          "                \"collectionURI\": \"http://gateway.marvel.com/v1/public/comics/60472/events\",\n" +
          "                \"items\": [],\n" +
          "                \"returned\": 0\n" +
          "            }\n" +
          "        }]\n" +
          "    }\n" +
          "}";

}