package aashrai.com.marvelcomicsearch.network.dto;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;

import aashrai.com.marvelcomicsearch.SampleData;

import static junit.framework.Assert.assertEquals;

/**
 * Created by aashrairavooru on 19/01/17.
 */
public class ComicSearchTest {
  @Before
  public void setUp () throws Exception {

  }

  @Test
  public void testGsonMapping () throws Exception {
    ComicSearch comicSearch = new Gson().fromJson(SampleData.sampleComicResponse, ComicSearch.class);
    assertEquals(20, comicSearch.getData().getCount());
    assertEquals(272, comicSearch.getData().getResults().get(0).getPageCount());
  }

}